﻿/// <binding />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    del = require('del'),
    Q = require('q');

var paths = {
	root: './wwwroot',
	front: './frontend',
	nodeM: './node_modules'
}

gulp.task('node-modules', function () {
	return gulp.src([
				paths.nodeM + '/core-js/client/shim.min.js',
				paths.nodeM + '/zone.js/dist/zone.js',
				paths.nodeM + '/reflect-metadata/Reflect.js',
				paths.nodeM + '/systemjs/dist/system.src.js',
				paths.nodeM + '/@angular/**/*'
			],
			{ base: './'})
		.pipe(gulp.dest(paths.root));
});

gulp.task('systemjs', function () {
	return gulp.src([
				'./systemjs.config.js'
			])
		.pipe(gulp.dest(paths.root));
});

gulp.task('clean', function () {
	return del(['wwwroot/**', '!wwwroot', '!wwwroot/app/**', '!wwwroot/index.html']);
});

gulp.task('copy:lib', ['clean'], function () {
    var libs = [
        "@angular",
        "systemjs",
        "core-js",
        "zone.js",
        "reflect-metadata",
        "rxjs"
    ];

    var promises = [];

    libs.forEach(function (lib) {
        var defer = Q.defer();
        var pipeline = gulp
            .src('node_modules/' + lib + '/**/*')
            .pipe(gulp.dest('./wwwroot/lib/' + lib));

        pipeline.on('end', function () {
            defer.resolve();
        });
        promises.push(defer.promise);
    });

    return Q.all(promises);
});

gulp.task('default', ['copy:lib', 'systemjs']);