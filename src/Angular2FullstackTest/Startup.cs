﻿using System;
using System.Linq;
using System.Reflection;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.DataAccess;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Services;
using Angular2FullstackTest.Api.Tools;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Angular2FullstackTest
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
            Mapper.Initialize(cfg =>
            {
                //мапинг из Api
                var apiAssembly = typeof(Book).GetTypeInfo().Assembly;
                foreach (var profile in apiAssembly.GetTypes().Where(t => typeof(Profile).IsAssignableFrom(t)))
                {
                    cfg.AddProfile(Activator.CreateInstance(profile) as Profile);
                }
            });

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IDataProviderFactory, AppDataProviderFactory>();

            //инъекция бизнес-слоя
            var businessAssembly = typeof(IBookService).GetTypeInfo().Assembly;
            var interfaces = businessAssembly.GetTypes().Where(t => t.GetTypeInfo().IsInterface && t.GetInterfaces().Any(i => i == typeof(IService))).ToList();
            var classes = businessAssembly.GetTypes().Where(t => t.GetTypeInfo().IsClass && t.GetInterfaces().Any(i => i == typeof(IService))).ToList();
            foreach (var inter in interfaces)
            {
                var type = classes.FirstOrDefault(t => t.GetInterfaces().Any(i => i == inter));
                if (type != null)
                    services.AddTransient(inter, type);
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IDataProviderFactory dataProviderFactory, IBookService bookService, ICategoryService categoryService)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseDefaultFiles(); // serves up our wwwroot files
            app.UseStaticFiles(); // allows serving of static files
            app.UseMvc();

            DbInitializer.Initialize(dataProviderFactory, bookService, categoryService);
        }
    }
}
