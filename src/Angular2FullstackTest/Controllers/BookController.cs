﻿using System;
using System.Collections.Generic;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.Common.Exceptions;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Dto;
using Angular2FullstackTest.Api.Services;
using Angular2FullstackTest.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

namespace Angular2FullstackTest.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class BookController : BaseController, IRestfulService<BookDto, Guid>
    {
        #region Fields

        private readonly IBookService _bookService;

        #endregion
        #region Constructors

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        #endregion
        #region IRestfulService Implementation

        /// <summary>
        /// Save(Create) book
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] BookDto item)
        {
            var book = new Book()
            {
                Title = item.Title,
                CategoryId = item.CategoryId
            };

            try
            {
                book = _bookService.Create(book);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<BookDto>(book);

            return Ok(result);
        }

        /// <summary>
        /// Get all books
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Book> books;
            try
            {
                books = new List<Book>(_bookService.GetAll());
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<IEnumerable<BookDto>>(books);

            return Ok(result);
        }

        /// <summary>
        /// Get single book
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            Book book;
            try
            {
                book = _bookService.Get(id);
                if (book == null)
                {
                    throw new BusinessException("Книга не найдена");
                }
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<BookDto>(book);

            return Ok(result);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="updatedItem"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] BookDto updatedItem)
        {
            var book = Mapper.Map<Book>(updatedItem);
            try
            {
                book = _bookService.Update(book);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<BookDto>(book);

            return Ok(result);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                _bookService.Delete(id);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }

            return Ok("Book is deleted");
        }

        #endregion
    }
}
