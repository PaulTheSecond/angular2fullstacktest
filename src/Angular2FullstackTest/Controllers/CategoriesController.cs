﻿using System;
using System.Collections.Generic;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.Common.Exceptions;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Dto;
using Angular2FullstackTest.Api.Services;
using Angular2FullstackTest.Controllers.Common;
using Microsoft.AspNetCore.Mvc;

namespace Angular2FullstackTest.Controllers
{
    [Route("api/[controller]")]
    public class CategoriesController : BaseController, IRestfulService<CategoryDto, Guid>
    {
        #region Fields

        private readonly ICategoryService _categoryService;

        #endregion
        #region Constructors

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        #endregion
        #region IRestfulService Implementation

        /// <summary>
        /// Save(Create) user
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromBody] CategoryDto item)
        {
            var Category = new Category()
            {
                Name = item.Name
            };

            try
            {
                Category = _categoryService.Create(Category);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<CategoryDto>(Category);

            return Ok(result);
        }

        /// <summary>
        /// Get all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<Category> Categorys;
            try
            {
                Categorys = new List<Category>(_categoryService.GetAll());
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<IEnumerable<CategoryDto>>(Categorys);

            return Ok(result);
        }

        /// <summary>
        /// Get single Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            Category Category;
            try
            {
                Category = _categoryService.Get(id);
                if (Category == null)
                {
                    throw new BusinessException("Книга не найдена");
                }
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<CategoryDto>(Category);

            return Ok(result);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="updatedItem"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] CategoryDto updatedItem)
        {
            var Category = Mapper.Map<Category>(updatedItem);
            try
            {
                Category = _categoryService.Update(Category);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }
            var result = Mapper.Map<CategoryDto>(Category);

            return Ok(result);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                _categoryService.Delete(id);
            }
            catch (BusinessException bEx)
            {
                return BadRequest(bEx.Message);
            }

            return Ok();
        }

        #endregion
    }
}
