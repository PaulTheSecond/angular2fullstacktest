﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Angular2FullstackTest.Controllers.Common
{
    public abstract class BaseController : Controller
    {
        protected IMapper Mapper = new Mapper(AutoMapper.Mapper.Configuration);
    }
}
