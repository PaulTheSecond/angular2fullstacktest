﻿var webpackMerge = require('webpack-merge');
var commonConfig = require('./webpack.common.js');
var helpers = require('./webpack.helpers');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-source-map',

    output: {
        path: helpers.root('wwwroot'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
});