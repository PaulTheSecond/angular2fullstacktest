﻿import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import {Routes, RouterModule} from '@angular/router';
import {HttpModule} from '@angular/http';

import { AppComponent }   from './app.component';
import { AddOrEditBookComponent }   from './addOrEditBook.component';
import { HomeComponent }   from './home.component';

import { APP_BASE_HREF } from '@angular/common'; //see in http://stackoverflow.com/questions/40274540/chrome-extension-no-base-href-set-please-provide-a-value-for-the-app-base-hre

// определение маршрутов
const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'book', component: AddOrEditBookComponent },
    { path: 'book/:bookId', component: AddOrEditBookComponent }
];

@NgModule({
    imports: [BrowserModule, RouterModule.forRoot(appRoutes, { useHash: true }), HttpModule, FormsModule],
    declarations: [AppComponent, HomeComponent, AddOrEditBookComponent],
    bootstrap: [AppComponent],
    providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
    ]
})
export class AppModule { }