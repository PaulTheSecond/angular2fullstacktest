﻿import {Injectable, Inject} from '@angular/core';
import {Http, Headers, RequestOptions, RequestMethod, Request, Response} from '@angular/http';
import {DOCUMENT} from '@angular/platform-browser';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HttpService {

    constructor(private http: Http, @Inject(DOCUMENT) private document) { }

    getData(serviceUrl: string, params: HttpParam[] = null) {
        if (!params) {
            return this.http.get(serviceUrl);
        }
        let queryString = serviceUrl + '?';
        params.forEach(param => {
            queryString += param.getParamStr();
        });
        return this.http.get(queryString);
    }

    postData(serviceUrl: string, data: any) {
        let hostName = document.location.hostname;
        let srvUrl = document.location.protocol + '//' + hostName + (hostName === 'localhost' ? ':51222' : '');
        
        let body = JSON.stringify(data);

        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

        return this.http.post(srvUrl + serviceUrl, body, { headers: headers })
            .map((resp: Response) => resp.json())
            .catch((error: any) => { return Observable.throw(error); });
    }

    updateData(serviceUrl: string, data: any) {
        let hostName = document.location.hostname;
        let srvUrl = document.location.protocol + '//' + hostName + (hostName === 'localhost' ? ':51222' : '');
        
        let body = JSON.stringify(data);

        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

        return this.http.put(srvUrl + serviceUrl + '/' + data.id, body, { headers: headers })
            .map((resp: Response) => resp.json())
            .catch((error: any) => { return Observable.throw(error); });
    }

    deleteData(serviceUrl: string, itemId: any) {
        let hostName = document.location.hostname;
        let srvUrl = document.location.protocol + '//' + hostName + (hostName === 'localhost' ? ':51222' : '');
        return this.http.delete(srvUrl + serviceUrl + '/' + itemId)
            .map((resp: Response) => resp)
            .catch((error: any) => { return Observable.throw(error); });
    }
}

export class HttpParam {
    name: string;
    value: string;
    getParamStr(): string {
        return this.name+"="+this.value;
    }
}