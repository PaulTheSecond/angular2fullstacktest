﻿import { Component } from '@angular/core';
import { HttpService } from './http.service';
import { Response } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'addOrEdit-app',
    templateUrl: 'addOrEditBook.component.html',
    providers: [HttpService]
})
export class AddOrEditBookComponent {
    book: Book;
    categories: any[];
    private _editMode: boolean = false;

    constructor(private httpService: HttpService, private router: Router, private route: ActivatedRoute) {
        this.book = new Book();
        let id = this.route.snapshot.params['bookId'];
        this.loadCats();
        if (id) {
            this.loadCat(id);
            this._editMode = true;
        }
    }

    save() {
        debugger;
        if (!this._editMode) {
            this.httpService.postData("/api/book", this.book).subscribe(resp => {
                    alert("Книга успешно сохранена");
                    this.router.navigate(["/"]);
                },
                err => console.log(err));
        } else {
            this.httpService.updateData("/api/book", this.book).subscribe(resp => {
                alert("Книга успешно обновлена");
                this.router.navigate(["/"]);
            },
                err => console.log(err));
        }
    };

    cancel() {
        this.router.navigate(["/"]);
    };

    private loadCats(): void {
        this.httpService.getData('/api/Categories').subscribe((data: Response) => this.categories = data.json());
    }

    private loadCat(id): void {
        this.httpService.getData('/api/book/'+id).subscribe((data: Response) => {
            let res = data.json();
            this.book.id = res.id;
            this.book.title = res.title;
            this.book.categoryId = res.categoryId;
        });
    }
}
export class Book {
    id: string;
    title: string;
    categoryId: string;
}