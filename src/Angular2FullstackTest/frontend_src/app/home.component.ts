﻿import {Component} from '@angular/core';
import {HttpService} from './http.service';
import {Response} from '@angular/http';
import {Router} from '@angular/router';
import {Book} from "./addOrEditBook.component";

@Component({
    selector: 'home-app',
    templateUrl: 'home.component.html',
    providers: [HttpService]
})
export class HomeComponent {
    books: any[];
    constructor(private httpService: HttpService, private router: Router) {
        this.loadBooks();
    }

    editBook(bookId:string): void {
        this.router.navigate(["/book", bookId]);
    }

    deleteBook(book: Book): void {
        if (confirm('Вы действительно хотите удалить книгу\''+book.title+'\'?')) {
            this.httpService.deleteData('/api/Book', book.id).subscribe(resp => {
                alert("Книга успешно удалена");
                this.loadBooks();
            },
                err => console.log(err));
        }
    }

    private loadBooks(): void {
        this.httpService.getData('/api/Book').subscribe((data: Response) => this.books = data.json());
    }
}