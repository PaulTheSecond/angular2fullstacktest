﻿using System;

namespace Angular2FullstackTest.Api.Dto
{
    /// <summary>
    /// Book DTO
    /// </summary>
    public class BookDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
        
        public Guid CategoryId { get; set; }

        public string CategoryName { get; set; }
    }
}
