﻿using System;

namespace Angular2FullstackTest.Api.Dto
{
    /// <summary>
    /// Category DTO
    /// </summary>
    public class CategoryDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
