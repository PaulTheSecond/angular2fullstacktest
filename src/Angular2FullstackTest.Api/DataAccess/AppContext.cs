﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Angular2FullstackTest.Api.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Angular2FullstackTest.Api.DataAccess
{
    public class AppContext : DbContext
    {
        #region Constructors
        
        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {
        }

        #endregion
        #region Properties

        public DbSet<Category> Categories { get; set; }
        public DbSet<Book> Books { get; set; }

        #endregion
        #region DbContext Implementation

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(m => m.Books)
                .WithOne(o => o.Category)
                .HasForeignKey(f => f.CategoryId);
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var validationErrors = ChangeTracker
                .Entries<IValidatableObject>()
                .SelectMany(e => e.Entity.Validate(null))
                .Where(r => r != ValidationResult.Success);

            if (validationErrors.Any())
            {
                Exception raise = null;
                
                    foreach (var validationError in validationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationError.MemberNames.Aggregate(string.Empty, (res, name) => res += name + "," ),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message);
                    }
                throw raise;
            }

            return base.SaveChanges();
        }

        #endregion
    }
}
