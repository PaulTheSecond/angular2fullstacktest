﻿using Angular2FullstackTest.Api.Common;
using Angular2FullstackTest.Api.Common.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Angular2FullstackTest.Api.DataAccess
{
    public class AppDataProviderFactory : IDataProviderFactory
    {
        private AppContext _context;
        private readonly DbContextOptionsBuilder<AppContext> _options;

        public AppDataProviderFactory(IConfiguration conf)
        {
            var connString = conf.GetConnectionString("app-database");
            _options = new DbContextOptionsBuilder<AppContext>();
            _options.UseSqlServer(connString);
        }

        public IUnitOfWork Create()
        {
            return new UnitOfWork(new AppContext(_options.Options));
        }
    }
}
