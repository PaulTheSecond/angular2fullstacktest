﻿using Angular2FullstackTest.Api.Common;
using Angular2FullstackTest.Api.Common.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Angular2FullstackTest.Api.DataAccess
{
    /// <summary>
    /// Обобщенный репозиторий для доступа к БД
    /// </summary>
    /// <typeparam name="TEntity">сущность домена</typeparam>
    public class AppRepository<TEntity> : BaseRepository<TEntity> where TEntity : class, IEntity
    {
        public AppRepository(DbContext context) : base(context)
        {
        }
    }
}
