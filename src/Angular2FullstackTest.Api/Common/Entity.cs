﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using Angular2FullstackTest.Api.Common.Contracts;

namespace Angular2FullstackTest.Api.Common
{
    public class Entity<TKey> : IEntity<TKey> where TKey : new()
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual TKey Id { get; set; } = new TKey();

        protected bool Equals(Entity<TKey> other)
        {
            return EqualityComparer<TKey>.Default.Equals(Id, other.Id);
        }

        public override int GetHashCode()
        {
            return EqualityComparer<TKey>.Default.GetHashCode(Id);
        }

        public object GetId()
        {
            return Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, this)) return true;

            var eobj = obj as Entity<TKey>;
            return eobj != null && Equals(eobj);
        }

        /// <summary>
        /// Получить описанеи сущности
        /// </summary>
        /// <returns>description</returns>
        public virtual string GetDescription()
        {
            var type = this.GetType();
            if (!type.GetTypeInfo().IsDefined(typeof(DescriptionAttribute)))
                return String.Empty;
            var attributeValue = type.GetTypeInfo().GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attributeValue?.Description ?? String.Empty;
        }
    }
}
