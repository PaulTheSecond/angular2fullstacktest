﻿using Microsoft.AspNetCore.Mvc;

namespace Angular2FullstackTest.Api.Common.Contracts
{
    public interface IRestfulService<in T, TKey>
    {
        /// <summary>
        /// CREATE
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        IActionResult Post([FromBody]T item);

        /// <summary>
        /// READ ALL
        /// </summary>
        /// <returns></returns>
        IActionResult Get();

        /// <summary>
        /// READ By ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IActionResult Get(TKey id);

        /// <summary>
        /// UPDATE
        /// </summary>
        /// <param name="updatedItem"></param>
        /// <returns></returns>
        IActionResult Put([FromBody] T updatedItem);

        /// <summary>
        /// DELETE
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        IActionResult Delete(TKey id);
    }
}
