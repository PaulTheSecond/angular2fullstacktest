﻿using System;
using Microsoft.EntityFrameworkCore;

namespace Angular2FullstackTest.Api.Common.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Call this to commit the unit of work
        /// </summary>
        void Commit();

        /// <summary>
        /// Return the database reference for this UOW
        /// </summary>
        DbContext Db { get; }
    }
}
