﻿namespace Angular2FullstackTest.Api.Common.Contracts
{
    public interface IDataProviderFactory
    {
        IUnitOfWork Create();
    }
}
