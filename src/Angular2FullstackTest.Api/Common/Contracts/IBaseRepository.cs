﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Angular2FullstackTest.Api.Common.Contracts
{
    public interface IBaseRepository<TEntity>
    {
        /// <summary>
        /// Retrieve a single item by it's primary key or return null if not found
        /// </summary>
        /// <param name="primaryKey">Prmary key to find</param>
        /// <returns>T</returns>
        TEntity GetById(object primaryKey);

        /// <summary>
        /// Returns all the rows for type T
        /// </summary>
        /// <returns></returns>
        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "");

        /// <summary>
        /// Does this item exist by it's primary key
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        bool Exists(object primaryKey);

        /// <summary>
        /// Inserts the data into the table
        /// </summary>
        /// <param name="entity">The entity to insert</param>
        /// <returns></returns>
        void Insert(TEntity entity);

        /// <summary>
        /// Inserts the data range into the table
        /// </summary>
        /// <param name="entities">The entities to insert</param>
        /// <returns></returns>
        void Insert(IEnumerable<TEntity> entities);

        /// <summary>
        /// Updates this entity in the database using it's primary key
        /// </summary>
        /// <param name="entityToUpdate">The entity to update</param>
        void Update(TEntity entityToUpdate);

        /// <summary>
        /// Updates or inserts entities in the database using it's identity expression, and sets the needs to update fields
        /// </summary>
        /// <param name="identifierExpression">identity expression</param>
        /// <param name="updatingExpression">sets the needs to update fields</param>
        /// <param name="entities"></param>
        void UpdateOrInsert(Expression<Func<TEntity, object>> identifierExpression, Expression<Func<TEntity, object>> updatingExpression, params TEntity[] entities);

        /// <summary>
        /// Deletes this entry fro the database
        /// ** WARNING - Most items should be marked inactive and Updated, not deleted
        /// </summary>
        /// <param name="entityToDelete">The entity to delete</param>
        void Delete(TEntity entityToDelete);

        /// <summary>
        /// Deletes this entry fro the database
        /// ** WARNING - Most items should be marked inactive and Updated, not deleted
        /// </summary>
        /// <param name="primaryKey"></param>
        void Delete(object primaryKey);
    }
}
