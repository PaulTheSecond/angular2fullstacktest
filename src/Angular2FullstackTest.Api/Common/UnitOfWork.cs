﻿using System;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.DataAccess;
using Microsoft.EntityFrameworkCore;
using AppContext = Angular2FullstackTest.Api.DataAccess.AppContext;

namespace Angular2FullstackTest.Api.Common
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppContext _context;

        public UnitOfWork(AppContext context)
        {
            _context = context;
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public DbContext Db => _context;
    }
}
