﻿using System;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Dto;
using AutoMapper;

namespace Angular2FullstackTest.Api.Mapping
{
    public class CategoryProfile: Profile
    {
        public CategoryProfile()
        {
            // Category -> CategoryDto
            CreateMap<Category, CategoryDto>();

            // CategoryDto -> Category
            CreateMap<CategoryDto, Category>()
                .ForMember(dest=>dest.Id, map=>map.Condition(cond=>cond.Id != Guid.Empty));
        }
    }
}
