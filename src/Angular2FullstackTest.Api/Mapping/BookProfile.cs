﻿using System;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Dto;
using AutoMapper;

namespace Angular2FullstackTest.Api.Mapping
{
    public class BookProfile: Profile
    {
        public BookProfile()
        {
            // Book -> BookDto
            CreateMap<Book, BookDto>()
                .ForMember(dest => dest.CategoryName, map => map.MapFrom(src => src.Category.Name));

            // BookDto -> Book
            CreateMap<BookDto, Book>()
                .ForMember(dest=>dest.Id, map=>map.Condition(cond=>cond.Id != Guid.Empty));
        }
    }
}
