﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Angular2FullstackTest.Api.Common;

namespace Angular2FullstackTest.Api.Domain
{
    [Description("Книга")]
    public class Book:Entity<Guid>, IValidatableObject
    {
        #region Properties

        [Required, MaxLength(256)]
        public virtual string Title { get; set; }

        [Required, Column("Category_Id")]
        public virtual Guid CategoryId { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        #endregion
        #region IValidatableObject Implementation

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            Validator.TryValidateProperty(this.CategoryId,
                new ValidationContext(this, null, null) {MemberName = "CategoryId" },
                results);
            Validator.TryValidateProperty(this.Title,
                new ValidationContext(this, null, null) {MemberName = "Title" },
                results);
            return results;
        }

        #endregion
    }
}
