﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Angular2FullstackTest.Api.Common;

namespace Angular2FullstackTest.Api.Domain
{
    [Description("Категория")]

    public class Category : Entity<Guid>, IValidatableObject
    {
        #region Properties

        public virtual string Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }

        #endregion
        #region IValidatableObject implementation

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();
            Validator.TryValidateProperty(this.Name,
                new ValidationContext(this, null, null) { MemberName = "Name" },
                results);
            return results;
        }

        #endregion
    }
}
