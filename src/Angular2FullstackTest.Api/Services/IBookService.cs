﻿using System;
using System.Collections.Generic;
using System.Linq;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.Common.Exceptions;
using Angular2FullstackTest.Api.DataAccess;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Services.Common;

namespace Angular2FullstackTest.Api.Services
{
    public interface IBookService : ICrudService<Book>
    {
    }

    public class BookService : AbstractCrudService<Book>, IBookService
    {
        #region Constructors

        public BookService(IDataProviderFactory dataProviderFactory) : base(dataProviderFactory)
        {
        }

        #endregion
        #region IBookService Implementation

        public override bool CheckIfExist(Book entity)
        {
            if (entity == null)
            {
                throw new BusinessException("Запись не должна быть пустой");
            }
            if (string.IsNullOrWhiteSpace(entity.Title))
            {
                throw new BusinessException("Не заполнено обязательное поле название");
            }
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<Book>(unitOfWork.Db);
                return Repository.Get(f => f.Title == entity.Title && f.CategoryId == entity.CategoryId).Any();
            }
        }

        public override IList<Book> GetAll()
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<Book>(unitOfWork.Db);
                var res = Repository.Get(null, null, "Category");
                return res.ToArray();
            }
        }

        public override Book Get(object id)
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<Book>(unitOfWork.Db);
                var res = Repository.Get(f=>f.Id == (Guid)id, null, "Category");
                return res.SingleOrDefault();
            }
        }

        #endregion
    }
}
