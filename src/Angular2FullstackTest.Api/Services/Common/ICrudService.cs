﻿using System;
using System.Collections.Generic;
using Angular2FullstackTest.Api.Common;
using Angular2FullstackTest.Api.Common.Contracts;

namespace Angular2FullstackTest.Api.Services.Common
{
    public interface ICrudService<TEntity> : IService where TEntity : Entity<Guid>
    {
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(object id);
        IList<TEntity> GetAll();
        TEntity Get(object id);
        bool CheckIfExist(TEntity entity);
        IEnumerable<TEntity> FindBy(System.Linq.Expressions.Expression<Func<TEntity, bool>> expression);
    }
}
