﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Angular2FullstackTest.Api.Common;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.Common.Exceptions;
using Angular2FullstackTest.Api.DataAccess;

namespace Angular2FullstackTest.Api.Services.Common
{
    public abstract class AbstractCrudService<TEntity> : ICrudService<TEntity> where TEntity : Entity<Guid>
    {
        #region Fields

        protected readonly IDataProviderFactory DataProviderFactory;
        protected AppRepository<TEntity> Repository;

        #endregion
        #region Constructors

        public AbstractCrudService(IDataProviderFactory dataProviderFactory)
        {
            DataProviderFactory = dataProviderFactory;
        }

        #endregion
        #region ICrudService<TEntity> implementation

        public virtual TEntity Create(TEntity entity)
        {
            if (CheckIfExist(entity))
            {
                throw new BusinessException($"Запись существует - {entity}");
            }

            entity.Id = Guid.NewGuid();

            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<TEntity>(unitOfWork.Db);
                Repository.Insert(entity);
                unitOfWork.Commit();
                return entity;
            }
        }

        public virtual TEntity Update(TEntity entity)
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<TEntity>(unitOfWork.Db);
                Repository.Update(entity);
                unitOfWork.Commit();
                return entity;
            }
        }

        public virtual void Delete(object id)
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<TEntity>(unitOfWork.Db);
                var entity = Repository.GetById(id);
                if (entity == null)
                {
                    var descr = string.Empty;
                    if (typeof (TEntity).GetTypeInfo().IsDefined(typeof (DescriptionAttribute)))
                    {
                        var attributeValue =
                            typeof (TEntity).GetTypeInfo().GetCustomAttribute(typeof (DescriptionAttribute)) as DescriptionAttribute;
                        descr = attributeValue?.Description ?? string.Empty;
                    }
                    throw new BusinessException($"Объект [{descr}] не найден.");
                }
                Repository.Delete(entity);
                unitOfWork.Commit();
            }
        }

        public virtual IList<TEntity> GetAll()
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<TEntity>(unitOfWork.Db);
                var res =  Repository.GetAll();
                return res.ToArray();
            }
        }

        public virtual TEntity Get(object id)
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<TEntity>(unitOfWork.Db);
                var res = Repository.GetById(id);
                return res;
            }
        }

        public abstract bool CheckIfExist(TEntity entity);

        public virtual IEnumerable<TEntity> FindBy(Expression<Func<TEntity, bool>> expression)
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<TEntity>(unitOfWork.Db);
                var result = Repository.Get(expression);
                return result.ToArray();
            }
        }

        #endregion //IServiceGroupService implementation
    }
}
