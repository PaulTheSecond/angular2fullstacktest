﻿using System;
using System.Collections.Generic;
using System.Linq;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.Common.Exceptions;
using Angular2FullstackTest.Api.DataAccess;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Services.Common;

namespace Angular2FullstackTest.Api.Services
{
    public interface ICategoryService : ICrudService<Category>
    {
    }

    public class CategoryService : AbstractCrudService<Category>, ICategoryService
    {
        #region Fields

        private readonly IBookService _userService;
        
        #endregion
        #region Constructors

        public CategoryService(IDataProviderFactory dataProviderFactory, IBookService userService) : base(dataProviderFactory)
        {
            _userService = userService;
        }

        #endregion
        #region ICategoryService Implementation

        public override bool CheckIfExist(Category entity)
        {
            if (entity == null)
            {
                throw new BusinessException("Запись не должна быть пустой");
            }
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<Category>(unitOfWork.Db);
                return Repository.Get(f => f.Name == entity.Name).Any();
            }
        }

        public override IList<Category> GetAll()
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<Category>(unitOfWork.Db);
                var res = Repository.Get(null, null, "Books");
                return res.ToArray();
            }
        }

        public override Category Get(object id)
        {
            using (var unitOfWork = DataProviderFactory.Create())
            {
                Repository = new AppRepository<Category>(unitOfWork.Db);
                var res = Repository.Get(f => f.Id == (Guid)id, null, "Books");
                return res.SingleOrDefault();
            }
        }

        #endregion
    }
}
