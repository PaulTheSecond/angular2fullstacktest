﻿using System;
using System.Linq;
using Angular2FullstackTest.Api.Common.Contracts;
using Angular2FullstackTest.Api.Common.Extensions;
using Angular2FullstackTest.Api.Domain;
using Angular2FullstackTest.Api.Services;

namespace Angular2FullstackTest.Api.Tools
{
    public static class DbInitializer
    {
        public static void Initialize(IDataProviderFactory dataProviderFactory, IBookService bookService,
            ICategoryService categoryService)
        {
            using (var uow = dataProviderFactory.Create())
            {
                uow.Db.Database.EnsureCreated();

                if (bookService.GetAll().Any())
                {
                    return;
                }
            }
            var cats = new Category[]
            {
                new Category { Id = Guid.NewGuid(),Name = "Детская литература"}, 
                new Category {Id = Guid.NewGuid(), Name = "18+"},
                new Category {Id = Guid.NewGuid(), Name = "Техническая литература"},  
            };


            cats.ForEach(f => categoryService.Create(f));

            var books = new Book[]
            {
                new Book
                {
                    Title = "Камасутра",
                    CategoryId = cats[1].Id,
                    Id = Guid.NewGuid()
                },
                new Book
                {
                    Title = "Как вынести Мозг и чтобы ничего за это не было",
                    CategoryId = cats[1].Id,
                    Id = Guid.NewGuid()
                },
                new Book
                {
                    Title = "Старик хоттабыч",
                    CategoryId = cats[0].Id,
                    Id = Guid.NewGuid()
                },
                new Book
                {
                    Title = "C++ для чайников",
                    CategoryId = cats[2].Id,
                    Id = Guid.NewGuid()
                },
                new Book
                {
                    Title = "Баня своими руками",
                    CategoryId = cats[2].Id,
                    Id = Guid.NewGuid()
                }
            };

            books.ForEach(f =>
            {
                bookService.Create(f);
            });


        }
    }
}
